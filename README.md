# Pathogen/Microbiome detection in RNA-seq samples with kraken2.

For microbiome study, we used the Kraken2 program based on its good results in sensitivity and computational performance when compared with other tools [https://doi.org/10.1093/bioadv/vbad014]. Kraken2 is a taxonomic sequence classifier based on k-mer that assigns taxonomic labels within a query sequence to those in the database. 

SOURCE: https://github.com/DerrickWood/kraken2/wiki/Manual#classification

Manual: https://jodyphelan.gitbook.io/tutorials/ngs/kraken2

## 1. System requirements
Disk space; 100 GB to build the STANDARD database \
Memory; 29 GB to hold the STANDARD database \
MiniKraken; kraken2-build with options --kmer-len and --minimizer-len and --max-db-size

## 2. Installation
```
git clone https://github.com/DerrickWood/kraken2.git
cd kraken2/
mkdir KRAKEN2_DIR
./install_kraken2.sh ./KRAKEN2_DIR/
sudo cp KRAKEN2_DIR/kraken2{,-build,-inspect} /usr/local/bin
git clone https://github.com/jenniferlu717/Bracken.git
cd braken2/
sudo cp them /usr/local/bin
git clone https://github.com/jenniferlu717/KrakenTools.git
cd krakentools/
sudo cp them /usr/local/bin
wget https://anaconda.org/bioconda/bracken/2.8/download/linux-64/bracken-2.8-py310h0dbaff4_1.tar.bz2
bunzip2 bracken-2.8-py310h0dbaff4_1.tar.bz2
tar -xvf bracken-2.8-py310h0dbaff4_1.tar
sudo cp kmer2read_distr /usr/local/bin
sudo apt-get install prinseq-lite
```

## 3. Database building

### 3.1 Ribosomal RNA sequence database.
SILVA provides comprehensive, quality checked and regularly updated datasets of aligned small (16S/18S, SSU) and large subunit (23S/28S, LSU) ribosomal RNA (rRNA) sequences for all three domains of life (Bacteria, Archaea and Eukarya).
Source: https://www.arb-silva.de/

```
mkdir SILVA
kraken2-build --db SILVA --special silva
```
### 3.2 FUNGI ref sequences database. 
```
mkdir FUNGI
kraken2-build --download-taxonomy --db FUNGI
kraken2-build --download-library fungi --db FUNGI
kraken2-build --build --db FUNGI
```
## 4. Reads as the input.  

### 4.1 Unmapped reads. 

```
for i in $(cat samples.list)
do
#Extract pair of reads non-mapping against Q.suber 
samtools view -bh -f12 ../${i}.markdup.bam > ${i}.unmapped.bam
#separate them in two files with FASTQ format.
samtools fastq ${i}.unmapped.bam -1 ${i}.unmapped.r1.fq -2 ${i}.unmapped.r2.fq
rm *unmapped.bam
#Filter by min-mean-base-quality 30,min-length 70 and filter out low-complexity sequences
#fastq-filter -e 0.001 -o ${i}.unmapped30.fq -o ${i}.unmapped30.fq ${i}.unmapped.r1.fq ${i}.unmapped.r2.fq
prinseq-lite -fastq ${i}.unmapped30.r1.fq -fastq2 ${i}.unmapped30.r2.fq \
-min_len 70 -min_qual_score 30 \
-lc_method dust -lc_threshold 7 \
#optional
-derep 12345
            Type of duplicates to filter. Allowed values are 1, 2, 3, 4 and
            5. Use integers for multiple selections (e.g. 124 to use type 1,
            2 and 4). The order does not matter. Option 2 and 3 will set 1
            and option 5 will set 4 as these are subsets of the other
            option.

            1 (exact duplicate), 2 (5' duplicate), 3 (3' duplicate), 4
            (reverse complement exact duplicate), 5 (reverse complement
            5'/3' duplicate)

-stats_info
-stats_len
-stats_dupl
rm *unmapped.r*.fq
```
### 4.2 All reads.
```
for i in $(cat samples.list) 
do 
kraken2 --db /media/vant/SHAULA/CRAG_CORK2WINE/7.\ RNASEQ/2.\ BAM/Q.suber/BAM/unmapped/16S_SILVA/ \
--report ${i}.silva.k2report \
--confidence 0.5 --minimum-base-quality --paired \
--gzip-compressed ${i}_1_val_1.fq.gz ${i}_2_val_2.fq.gz > ${i}.unmapped30.silva.kraken2
done
```

## 5. Good quality and pairs unmapped reads classification.

### 5.1 Classification in the three domains of life (Bacteria, Archaea and Eukarya) by rRNA SILVA database.
Clean and unmapped against Q.suber transcriptome sequences were addressed to SILVA ribosomal RNA database to obtain a first evaluation of the fraction of non-plant reads that might be present in the data. 
```
cd SILVA
mkdir conf05

kraken2 --db 16S_SILVA --report SILVA/conf05/${i}.silva.k2report \
--confidence 0.5 --report-minimizer-data --paired \
--classified-out SILVA/conf05/${i}.unmapped30.silva#.fq \
${i}.unmapped30.r1.fq ${i}.unmapped30.r2.fq > SILVA/conf05/${i}.unmapped30.silva.kraken2
```
### 5.2 Classification inside FUNGI taxonomic group by ref sequences. 
```
cd FUNGI
mkdir conf05

kraken2 --db FUNGI --report FUNGI/conf05/${i}.fungi.k2report \
--confidence 0.5 --paired \
--classified-out FUNGI/conf05/${i}.unmapped30.fungi#.fq \
--report-minimizer-data \
${i}_prinseq_good_R1.fastq ${i}_prinseq_good_R2.fastq > FUNGI/conf05/${i}.unmapped30.fungi.kraken2
```
## 6. Results visualization 
Pavian is a tool for interactive analysis of metagenomics classification results. Read more about it in the Preprint or its vignette. It's built on R and Shiny, and supports Kraken, Centrifuge and MetaPhlAn report files. Please note that currently the default Centrifuge report format is not supported. To generate a compatible report, use the script centrifuge-kreport that is distributed with Centrifuge. 

upload *.k2report files
https://fbreitwieser.shinyapps.io/pavian/
